# Proyecto_1_MiguelFranSiro

## Suggestions for a good README
The scraping codes for the Airbnb pages and some supplementary ones are attached.

## Name
Airbnb database

## Description
Scraping Tool for Airbnb and Analysis Conducted on Sample Collection Dated 08/06/2023

## Visuals
Graphics included within the Python code and an image attached in .PNG format.

## Installation
It is necessary to have Python notebook or a similar environment installed in order to modify and execute the code.

## Usage
numpy, pandas, matplotlib.pyplot, seaborn, plotly.express, datetime, csv, folium, geopandas

## Support
In case of any issue or unforeseen circumstance, you can always count on the unwavering assistance of our mentor, Fede.

## Contributing
Any contribution will be welcome, and we will strive to make the best use of every cent.

## License
Free for all. ;)
